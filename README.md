# weekly

取乎其上，得乎其中；取乎其中，得乎其下；取乎其下，则无所得矣

短为周，长为月，一也

## 2019

**八月**：[第十二期](docs/issue-11.md)

**七月**：[第十一期](docs/issue-10.md)

**六月**：[第九期](docs/issue-9.md)

**五月**：[第八期](docs/issue-8.md)

**四月**：[第七期](docs/issue-7.md)

**三月**：[第六期](docs/issue-6.md)

**二月**：[第五期](docs/issue-5.md)

**一月**：[第四期](docs/issue-4.md)

## 2018

**十二月**：[第二期](docs/issue-2.md)  [第三期](docs/issue-3.md)

**十一月**：[第一期](docs/issue-1.md)