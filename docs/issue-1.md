# 分享第一期

这里记录过去最长一个月、最短一周，我看到的值得分享的东西。

![ref](../picture/祈福-1.jpg)

吾日三省吾身：为人谋而不忠乎？与朋友交而不信乎？传不习乎？

## 新奇

1、[游民星空开发者圈子](https://www.gamersky.com/zhuanti/indieapply/)

![游民星空开发者圈子](http://image.gamersky.com/webimg15/logo/chang/160x53.png)

从高中下载盗版游戏开始一直陪伴着我到工作的网站。现在，他们希望能在这些玩家和游戏开发者们之间建立一个更加便于沟通交流的桥梁。开发者们可在通过这个的链接申请入驻圈子。双赢的政策。

2、[健康的作息时间表](https://mp.weixin.qq.com/s/KZovvsVWWyTEdE_FI_yUEQ)

![健康的作息时间表](http://06imgmini.eastday.com/mobile/20181123/20181123065851_b64bf854306a9a3e52fb203a89538d0c_1.jpeg)

>午后是人思维最活跃的时间，非常适合做一些创意性的工作。
>早上学习工作的最佳时间，头脑最清醒，思路最清晰的时间段。

一个作息时间的建议。理论上已说明每个人都是不一样的，但是还是很有参考意义，毕竟身体是革命的本钱。

3、**非自然死亡**

法医题材不算新鲜，但紧凑的叙事和折射出的日本社会现状，让该剧成为了一部有吸引力的剧集。豆瓣评分挺高的。

4、**社会主义核心价值观**

![社会主义核心价值观](https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1543215307&di=bf51f8f2039bf9a7c28be51c7863b4c5&src=http://pic.58pic.com/58pic/16/75/35/56P58PICKnd_1024.jpg)

马克思列宁主义。

5、[废弃式设计风格](https://www.atlasobscura.com/articles/best-superstores-architecture)

![废弃式设计风格](https://assets.atlasobscura.com/article_images/lg/58011/image.jpg)

上个世纪70年代，美国家居用品零售商 Best Products 店铺都采用废弃式的设计，看上去建筑物未完工或已经废弃了，但实际上是正常使用的。

6、**一句话新闻**

* 古剑奇谭三发售。
* 据称基因编辑婴儿在中国出生。切记敬畏生命。
* 合着门的书架设计。
 ![书架](https://mmbiz.qpic.cn/mmbiz/dDMv1sFIW77bmHia7KiafFjPbhX97WWTuAvDeXF7Aa0Gc7FEPIvOOgxXUykJLqlZ46WibJ1zicibnZR80j38HO6FAYQ/640?tp=webp&wxfrom=5&wx_lazy=1&wx_co=1&retryload=1)

## 教程

1、[git push --force-with-lease](https://blog.csdn.net/wpwalter/article/details/80371264)（中文）

不要用 git push --force，而要用 git push --force-with-lease 代替。在你上次提交之后，只要其他人往该分支提交给代码，git push --force-with-lease 会拒绝覆盖。

2、[网页设计的常见错误](http://blog-en.tilda.cc/articles-website-design-mistakes)（英文）

![网页设计的常见错误](https://camo.githubusercontent.com/dddbd6fe8f27195db81c3b45ae807079bbdf28b2/68747470733a2f2f7777772e77616e67626173652e636f6d2f626c6f67696d672f61737365742f3230313830382f6267323031383038333131312e6a7067)

本文使用图片对比，列出网页外观设计的15个注意点。上图左侧是改造前的网页，右侧是改造后。

3、[代码中常见的判断](https://code.joejag.com/2016/anti-if-the-missing-patterns.html)

一个说明在代码里面为什么不要滥用 if、switch case、boolean等常见的语法，以及怎么处理。

4、[10道家常菜的做法](http://mmbiz.qpic.cn/mmbiz/SP9wJM6XE5ZW1DjBGW98q5ZJw2DGbRbufU8fwx8zXAJBMtzqo0SXhL9rpUpRoXHgydIOSejMPqXTPeHtboG4vw/640?tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

![10道家常菜的做法](http://mmbiz.qpic.cn/mmbiz/SP9wJM6XE5ZW1DjBGW98q5ZJw2DGbRbufU8fwx8zXAJBMtzqo0SXhL9rpUpRoXHgydIOSejMPqXTPeHtboG4vw/640?tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

教你花几分钟时间轻松烹饪十道家常菜。健康美味又省钱。

5、[学会十道菜，不用下馆子！](https://mp.weixin.qq.com/s/kIoAMsNjVVuUi9eu0GtvAQ)

![学会十道菜，不用下馆子！](http://mmbiz.qpic.cn/mmbiz_jpg/DhKPeHFI5HiaduXGN7rhp3FQxryX2IXaRsTfcGb3LdOGsGT37IJTbG92lOicsG0Zh6WPFh9PHN20WKHqm8J8Tk3A/640?tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

经典美味，值得看一看怎么做的。

6、[Puppeteer 网页爬虫和自动化测试教程](https://github.com/csbun/thal)（中文）

想要学习浏览器自动化的同学，可以看看这篇中文教程。

7、[JCSprout](https://github.com/crossoverJie/JCSprout)

一个收集 Java 核心知识的中文库。

8、[Favioli](https://favioli.com/)

![Favioli](https://favioli.com/assets/screenshots/comparison.png)

Favioli 是一个很好玩的 Chrome 浏览器插件。它可以将网页的 Favicon 替换成 Emoji。

9、[puppeteer-recorder](https://chrome.google.com/webstore/detail/puppeteer-recorder/djeegiggegleadkkbgopoonhjimgehda)

Chrome 插件，可以将用户在浏览器里面的操作，自动生成对应的 Puppeteer 脚本。

## 资源

1、[Go 高级编程](https://github.com/chai2010/advanced-go-programming-book)

![高级编程](https://camo.githubusercontent.com/86b70d0783175a71ded9a99f7d5c2691b81a3c2b/68747470733a2f2f7777772e77616e67626173652e636f6d2f626c6f67696d672f61737365742f3230313830382f6267323031383038333131392e6a7067)

开源电子书，涵盖CGO、Go汇编语言、RPC实现、Web框架实现、分布式系统等高阶主题。

2、[少说太忙，习近平劝你多读书](https://mp.weixin.qq.com/s/EY31u4GbccoriL_J9bJ7Qg)

![习近平的书库](http://mmbiz.qpic.cn/mmbiz_jpg/xrFYciaHL08CvLfYUcBC33RxQCXmoCk2qA5HOPekFO7nbAeSGJY7qVH6ZbTW5YPhSnzoY8jVvvyh789O40Zt8QQ/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

>今天，不仅仅是文艺工作需要从书中汲取力量，我们生活和工作的方方面面也需要书籍的滋养。就让我们一起看看，关于读书，习近平是怎么说的。

总书记，站在权利巅峰的人。

3、[机器学习训练秘籍](https://mp.weixin.qq.com/s/dJW6TZCZne_7jLwd7VoW2Q)

![机器学习训练秘籍](https://mmbiz.qpic.cn/mmbiz_png/GBGeoiaGMOv37B7ceyAleiaoTO0Hkm3yajp13ZlCdhQuVE1laLYpUSgYH3rTGvQyvlQBdspHNArIu9hlslsTmpKw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

>唯一比成为超级英雄更好的事情就是成为超级英雄团队的一员。 我希望你能把这本书的副本交给你的朋友和队友，并帮助创造出其他的超级英雄！

一本关于AI的书籍。

4、[《计算与推断：数据科学基础》](https://ds8.gitbooks.io/textbook/content/)（英文）

>本书是加州大学伯克利分校《数据科学导论》课程的教材，现在开源了。

## 工具

1、[爱盘 - 在线破解工具包](https://github.com/ganlvtech/down_52pojie_cn)

本仓库为吾爱破解论坛爱盘页面的源代码。爱盘 down.52pojie.cn。

## 阅读

1、**《解除限速》**

![解除限速](https://gss1.bdstatic.com/-vo3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=6851efb205f41bd5da53eff269e1e6f6/d439b6003af33a87fc1f0883cd5c10385343b53c.jpg)

他就是美国推理小说作家，当代硬汉派侦探小说大师劳伦斯·布洛克。
如果你还没看过他的小说，那么不妨从这本中篇小说集开始。

2、**《衣的现象学》**

![衣的现象学](https://gss0.bdstatic.com/94o3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=90f40963b7a1cd1105b675268129afc1/8644ebf81a4c510fb1d10f496d59252dd42aa548.jpg)

时尚，是一种让人摸不着头脑的东西。奥斯卡•王尔德说：“人要么成为艺术作品，要么就穿着艺术作品。除此以外，没有别的选择。”

3、**《激荡十年，水大鱼大》**

![激荡十年，水大鱼大](https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=d43ddebd8d18367aad8978db1648ece9/b58f8c5494eef01f5e450b51ebfe9925bd317dd4.jpg)

吴晓波从历史的角度记录了中国从2008-2018十年间的变迁。读过本书，或许你会对个人和社会的脉络又更深的体会。正如吴晓波在本书自序中所言：除非经由记忆之路，人不能抵达纵深。

4、**《日本的洗手間及其它》**

挺有意思的一讲厕所体验的本书，当初忘了在哪看到这本书的介绍了，网上百度了一大圈都没有找到。

5、**《瓦尔登湖》**

![瓦尔登湖](https://gss2.bdstatic.com/9fo3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=ecfa686ba76eddc426e7b3fd01e0d1c0/3bf33a87e950352a245263865943fbf2b2118b09.jpg)

>《瓦尔登湖》是美国作家梭罗独居瓦尔登湖畔的记录，描绘了他两年多时间里的所见、所闻和所思。该书崇尚简朴生活，热爱大自然的风光，内容丰厚，意义深远，语言生动。

值得一看。

6、[写作技术文档的技巧](https://blog.stoplight.io/writing-documentation-when-you-arent-a-technical-writer-part-one-ef08a09870d1)

用户阅读网页内容的热力图是下面这样。

![写作技术文档的技巧](https://camo.githubusercontent.com/2dbad53b3130196123c25b53cc4c04a40c278d44/68747470733a2f2f7777772e77616e67626173652e636f6d2f626c6f67696d672f61737365742f3230313830392f6267323031383039313432352e6a7067)

这就是说，用户以 F 状的方式阅读网页，先看前三行，然后垂直向下阅读，只看每一行的前几个字。

所以，写作的时候，应该注意下面几点。

>* 第一段和第二段必须给出最重要的信息，而且第一句话最重要。
>* 标题、段落、列表的开头，都应该立即给出信息。
>* 通过字型的变化（大小、黑体、链接），把用户的注意力吸引到重点句子。

7、[给玩家选择的余地——让游戏也有个进度条](https://mp.weixin.qq.com/s/2TvjQKLQ7gF_lVTHogf4CQ)

![给玩家选择的余地——让游戏也有个进度条](https://mmbiz.qpic.cn/mmbiz_jpg/uicUKliaDticRa9iaNexFTAkSpM8PdghlRKAicfp1RSeuqnFLrSKezf8hg0y5vxdBCslEg6nXAaXPhjczRJFdicSlT4A/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

很有趣的一篇文章。其中观点游戏应该让玩家可以跳过不喜欢的部分，直接开始喜欢的部分，我觉得还是很有道理的。

## 金句

1、

时间可以治愈一切，只是有时候剂量大到需要一辈子，药效结果叫“算了”。

2、

我在大学没什么关系好的同学，跟那些好书一比，身边的人都显得很平庸、肤浅······我不觉得读书无用，整个社会都是由读书人撑起来的。（曾在校成绩很好的落魄中年人回忆）

3、

小时候以为早睡早起身体好是一句口号。长大后才发现，那是三个愿望。

## 正文到此结束

读书以前总觉得生命是脆弱的，之后又觉得应该是短暂的。脆弱又短暂，眼看又快新的一年，想起了自己多年前写的一句话——时间略久，我们便死了。