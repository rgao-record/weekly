# 分享第五期

1、[github私有库免费了](https://github.com/)

良心更新了，还是有钱好哟。

2、[国家税务APP](https://itunes.apple.com/cn/app/id1038364110?mt=8)

> 未来最懂你的人，是税务。

## 好文

### 1、[2018 年，这些硬件、App、书影音最让我们满意](https://mp.weixin.qq.com/s/vpoDk2gCL-foIIX6_-phQg)

`物品`

> 1. 埃普笔记本支架便携式电脑支架
> 2. 日本仓敷意匠画室 drop around 便签条
> 3. muurla 卡通马克杯
> 4. 暹粒夜市的明信片，他们家的店叫「Lily Shop」，所有明信片都是自己原创设计画风很舒服如果去吴哥窟玩
> 5. 得力 2050 便携开箱器

`音乐`《If I Go, I Goin》

`电影`《犬之岛》、《四个春天》、《利兹与青鸟》

`书籍`

> - 《Creative Selection》、《Fear》、《Bad Blood》
> - 《作文七巧》 - 不同类型的写作方法
> - 《东京本屋》 - 日本经营一家书店的方方面面
> - 《无印良品生活研究所》 - 对生活方式的各种答案和总结

### 2、[从游戏王和三国杀对比看桌游模块化设计](http://gad.qq.com/article/detail/289078?ADTAG=gad.ch.wz)

![从游戏王和三国杀对比看桌游模块化设计](http://gadimg-10045137.image.myqcloud.com/20190114/5c3c37eb2b478.jpg)

比较详细分析了两款卡牌游戏的异同，得出卡牌游戏都是由差不多足够细化的模块组合来的游戏，借此作者希望游戏设计者能够举一反三设计出有自己的特色的游戏模块。

卡牌几要素：

- 基本牌：基本牌是一款卡牌游戏的核心，是卡牌游戏灵魂的所在。一个是构成属性及使用规则简单。二是其他大部分卡牌的使用都无法与基本牌脱离关系。
- 卡牌效果：卡牌效果分为正面效果和负面效果两种
- 效果判定：硬币（正反面），六面骰（六个数字），卡牌花色（根据游戏而定）
- 指示物
- 牌堆
- 抽卡辅助
- 手牌破坏
- 弃牌回收
- 卡牌限制
- 效果无效

其中有个观点跟我想的很一致那就是`游戏模块`

### 3、[教你用认知和人性来做最棒的程序员](https://mp.weixin.qq.com/s/C_1puTYAYjjFS2MfUjXaRg)

从来认为最字是最不好用的，但是这篇文章的观点，我很赞同，因为我也是这么想的。
>持之以恒地实践
>
>让我们不要再局限于程序员狭义技术的范畴内，把提升自己的认知作为最重要的目标，我们要努力做到“既是程序员，也不是程序员”。

通过文章自己我分析，想想自己为什么还是难以进步,下面这句话我痛：

>人就是一个如此奇妙，如此复杂的生物，不管你看多少书，看多少源码，写多少demo，你不真刀真枪地去实践，去写代码，这些知识无论如何都无法进入你大脑的知识图谱。它们永远只能是“狭义上的知识”，而不是“有价值的认知”。

### 4、[这么大的狗能进客舱？](https://mp.weixin.qq.com/s/N6uRT9eOJ7RzkPAwLSwe7g)

动物也可以是人类的好朋友，工作犬（包括辅助犬、导听犬、导盲犬）用处可真大。

### 5、[“夫妻相”确有其事？这位心理医生给出了完美解释](https://mp.weixin.qq.com/s/BakgMDTYw1xsU02Y48kYbQ)

学会 **懂得**
>“懂才是爱的前提”

## 教程

### 1、[Learn touch Typing for free](https://www.typingclub.com/?tdsourcetag=s_pctim_aiomsg)

锻炼打字的网站，想挑战自己吗？少年还等什么。

### 2、[三张表](https://mp.weixin.qq.com/s?__biz=MzA5MDg0NjY0Mw==&mid=2649611912&idx=1&sn=442955b548079b46ef3e438104e33ff3&chksm=881c1a54bf6b9342c647f26befe8df3fd3e9fc2cfa95415b3789457f9e873b017610eaaefa45&scene=21#wechat_redirect)

那么一个家庭创造财富、守护财富的关键，就是管理好最核心的三张表。

![三张表](https://mmbiz.qpic.cn/mmbiz_png/ibw8b04QVx2PZo9AUOicGach6epzlAnAjib6j09oTEjB1zXaIHQ5h8bhb30QKNb1KrthQfbT9LRN9wzg7T2MTJdkQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

![三张表](https://mmbiz.qpic.cn/mmbiz_png/ibw8b04QVx2PZo9AUOicGach6epzlAnAjiba62gQNeOSHHN6kkaKl22ZeCU38D4WbqcxA0n1yOiaGHjLeNYEwWPTKw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### 3、[学会管理精力，让每天都充满干劲！](https://mp.weixin.qq.com/s/DKv4oK5WS7waq2XM86Cm1w)

很关键！按照说的去做，耐心总不会错。

## 资源

### 1、[干货 | 剧情创作七要素，你掌握了哪几个？](https://www.sohu.com/a/224952157_441383)

作者认为，主要包括以下7大要素，分别为：【世界观】、【哲学主题】、【故事主题】、【情绪主题】、【主线剧情结构】、【主要人物设定】、【矛盾&冲突面】，每个主题下面还可以细分。

提到一本书籍
`《千面英雄》`

## 工具

### 1、[MarkdownPad2 Pro](https://www.52pojie.cn/thread-849764-1-1.html)

一款用Markdown写作的软件

### 2、[imgcook](https://imgcook.taobao.org/)

设计稿一键智能生成代码，看起来挺不错的。

### 3、[数据可视化工具](https://mp.weixin.qq.com/s/khi_LTxtSsGbtcI3Ej8LIg)

## 金句

1、

生活从来不易，幸福却处处埋下伏笔

2、

一生，一事

3、

idea is cheap

4、

平视而不是怜悯，帮助而不是施舍。

## 正文到此结束

这几天脑海中老是重复`“你又是何必呢”`这句话。

我早该明白的，`时间在犹豫不决中流逝`，给自己定个小目标

- 坚持理财
- 最少也要做个游戏demo