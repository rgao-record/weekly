# 分享第六期

[机器人皮肤](https://news.yale.edu/2018/09/19/robotic-skins-turn-everyday-objects-robots)
耶鲁大学的科学家发明了一种可卷曲的"机器人皮肤"，可以包裹在任何物体的外层，使得该物体变成可以控制的机器人。上图是毛绒填充的小马玩具穿上"机器人皮肤"以后，变成可以走动了。

科学家已经实现了，给气球穿上"机器人皮肤"，使得气球变成可以编程控制。

## 好文

1、[2018 年，这些硬件、App、书影音最让我们满意](https://mp.weixin.qq.com/s/vpoDk2gCL-foIIX6_-phQg)

比较好用的管理桌面的软件：xyplorer

2、[卡牌游戏运营期间如何降低“贫富差距”？《火影忍者OL》手游给出了一个答案](https://mp.weixin.qq.com/s/1EBv2uvwiawfns002R3lWw)

battle pass 方式：根据玩家活跃度来给与玩家奖励的方式，玩家通过购买“通行证”来增加自己的活跃度奖励，可以自己决定是否充值小额度。

3、[游戏关卡设计自检标准：没有这四个元素你的设计一定有问题](https://mp.weixin.qq.com/s/fCmYrdskv74cVzx5aNj0Fw)

1. 故事
2. 目标&阻碍
3. 反馈机制
4. 社交互动

4、[这是一份获诸多好评【数值策划的年终总结】](https://mp.weixin.qq.com/s/U6kHWT4Xrm9-Qbo5LjphJQ)

关于游戏中：	1. 属性架构
			2. 怪物属性数值
			3. 战斗关卡设计
			4. 技能设计
			5. 不同情景的收益差异

5、[最近面试 Java 后端开发的感受！](https://mp.weixin.qq.com/s/9lL1kQqsRGSx_DH_DF-liQ)

面试官的感受，值得一看，很有指导意义，能够增加面上几率。

6、[告别野路子，小游戏运营这么做——游戏内交叉推广](https://mp.weixin.qq.com/s/kl_6SpxdgV0iMAS5qaLTTQ)

一片介绍游戏内交叉推广好处的文章。

7、[关于“天赋树” 游戏设计师想了解的可能在这里](https://mp.weixin.qq.com/s/gbPaCeuDouVgzHLJdaCWkw)

一款ARPG游戏最能吸引我的就是关于天赋树了,这篇文章很好的介绍和思考了相关的问题。

[灵感从哪里来？ 三个技巧让游戏设计更有新意](https://mp.weixin.qq.com/s/_VY92GE8vEol-OnuYmjg3w)

创新需要的能力 
1. 发现问题的能力
2. 解决问题的能力
3. 实践与改进的能力
4. 学会强势批判性思维
5. 学更多的知识，开更多的脑洞
6. 磨练自己敢于去做

[掌握这几套公式，实现任意两职业的“完全平衡”](https://mp.weixin.qq.com/s/7kKOgaVVFW383n1wizo3Xg)

文章指的就是一种减法公式，两种（乘）除法公式；

## 教程

 1、[Learn touch Typing for free](https://www.typingclub.com/?tdsourcetag=s_pctim_aiomsg)

练习打字的网站

 2、[MySQL 动态查询](https://mp.weixin.qq.com/s/mxcP2P3pePYFrXbiOOP-0g)

MyBatis 的高级用法

## 资源

1、[剧情创作七要素，你掌握了哪几个？](https://www.sohu.com/a/224952157_441383)

文章讲的是【世界观】、【哲学主题】、【故事主题】、【情绪主题】、【主线剧情结构】、【主要人物设定】、【矛盾&冲突面】七要素。

2、[真经阁丨在资源有限时，如何设计出可重复挑战的游戏关卡？](https://mp.weixin.qq.com/s/yrLLmmKijxLJrfQ8-eTqYw)

讲述如何着手做好重复挑战的游戏关卡（游戏随机性？！）的文章。

重要规则：
- 

3、[国内首部 | 《游戏学》：是时候认真看待游戏了！](https://mp.weixin.qq.com/s/CbsDUMGqASD0_KVmNnWEhQ)

《游戏学》这本书还是很期待阅读

4、[LeetCode](https://leetcode-cn.com/explore/)

算法与数据结构缺一不可。为了人类···

5、[Java必备的几个开发工具，你会几个？](https://mp.weixin.qq.com/s/E5QcWjj-5n0pvPgGdOwUkw)

- 开发工具篇
- Maven 篇
- Git 篇
- 其他工具篇

## 工具

1、[34 款 Windows 软件中的神器：看片、安全杀毒、效率神器，它们最好用](https://mp.weixin.qq.com/s/3e6gk3K3tLZXFegUc3O0pA)

不错的软件 Visual Studio Code

2、[sqlfmt](https://sqlfum.pt/)

SQL 语句格式化的在线工具。

## 金句

1、

很多面试官喜欢问你在这个项目中遇到的困难是什么，又是怎么解决的

2、

游戏无界-游戏无界有两层意思：一是游戏开发者的思想和创意无界，二是游戏超越了地域和国界的局限。

## 正文到此结束

公司在提升级。两年多了，顺便整理了下都学到了些什么

CDS，中退票相关接口开发 微服务框架 postman测试
PP，项目 React框架 ES6规范 echarts绘图 antDesign
auto-util，项目开发系统切换功能等，webSocket React
psger-filler，团队前台自动加人插件维护，打包成插件
自动测试 ，前端仓位航班选择，前后端订单查询，Xpath gauge
生产，自动进程 SHELL，前台 struts2 后台 angulrJs SpringMVC AJAX
上线，FT 产看日志，简单的 Linux指令
需要加强基础知识算法与数据结构
希望能够坚持冥想

V3版本 微服务改造
java环境 JDK 1.8
开发工具 ide
包管理 maven
版本控制 git
验收测试 gauge
日志 logback
开发框架 springboot jersey
进行参数合法检查 BeanValidation 
实现资源保护 Hystrix
规范 RestfulAPI
进行应用日志收集 Elasticsearch
处理异常 SpringExpectionHanrd
模块化 springBoot
模块间的调用 总的来说 写操作调用jar包

插件框架：
图形框架：

1、 jvm 调优

查看gc日志 使用可视化工具gceasy,tomcat 下面查看，-Xms添加内存

2、 Memcached 框架

分布式的高速缓存系统，无备份一旦某个节点丢失，缓存中的数据也会随之丢失，命令基本就是对特定关键部分进行添加，删除，替换，原子更新，读取等。可以做一些原子命令的操作。

2.Struts2的核心是什么,体现了什么思想
   答:Struts2的核心是拦截器,基本上核心功能都是由拦截器完成,拦截器的实现体现了AOP(面向切面编程)思想?

4 Struts2 如何定位action中的方法
    1 感叹号定位方法（动态方法）。
    2 在xml配置文件中通过配置多个action，使用action的method指定方法。
    3 使用通配符(*)匹配方法。

23.struts2的请求处理流程
    答：
    1.客户端发送请求。
    2.经过一系列的过滤器(如:ActionContextCleanUp、SiteMesh等)到达核心控制器(FilterDispatcher)。
    3.核心控制器通过ActionMapper决定调用哪个Action，如果不是调用Action就直接跳转到jsp页面。
    4.如果ActionMapper决定调用了某个Action，核心控制器就把请求的处理交给一个代理类(ActionProxy)。
    5.代理类通过配置管理器(Configuration Manager)找到配置文件（struts.xml）找到需要调用的Action类。
    6.代理类还要创建一个Action的调度器(ActionInvocation)。
    7.由调度器去调用Action，当然这里还涉及到一些相关的拦截器的调用。
    8.Action执行完后，这个调度器还会创建一个Result返回结果集，返回结果前还可以做一些操作(结果集前的监听器)。

 什么是J2EE
  J2EE是Java2平台企业版（Java 2 Platform,Enterprise Edition），它的核心是一组技术规范与指南，提供基于组件的方式来设计、开发、组装和部署企业应用。J2EE使用多层分布式的应用模型。

jersey
对于请求式服务，对于GET,DELETE请求，你甚至只需要给出一个URI即可完成操作。

3、什么是Spring Boot呢？
       Spring Boot基本上是Spring框架的扩展，它消除了设置Spring应用程序所需的XML配置，为更快，更高效的开发生态系统铺平了道路。
       特点：绝对没有代码生成和XML配置要求，嵌入Tomcat, Jetty Undertow 而且不需要部署他们。尽可能自动配置spring应用。
       Spring Boot只是Spring本身的扩展，使开发，测试和部署更加方便。
       Spring Boot应用程序的入口点是使用@SpringBootApplication注释的类：
