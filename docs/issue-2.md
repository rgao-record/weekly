# 分享第二期

![ref](../picture/祈福-2.jpg)

## 新奇

1、[9岁开始学就晚了? 这个学费上万仍备受“追捧”的课程](https://mp.weixin.qq.com/s/P67VQV8kRufDd-MbtHP1OA)

![这个学费上万仍备受“追捧”的课程，你家孩子上过吗](https://mmbiz.qpic.cn/mmbiz_jpg/oq1PymRl9D4jficTEOsVRTLXAE8QXvc468xEE3b7WncicPicJ0Qibx6iakJcjpMF5KhgJoTW1gHslSYOJcQesen6Klg/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1&retryload=1)

据报道现在很多小孩子都去报少儿编程培训班，我觉得这是个商机。

2、[人造肉](https://mp.weixin.qq.com/s/om7Km63jiksET2pMYp34Aw)

![人造肉](https://mmbiz.qpic.cn/mmbiz_gif/oq1PymRl9D62VYcibdiancbb3ibCTx11mJfYzSXibIhicPSib9U9AhTmQicrLUFVZqcORWtm94gNL2vfptzTJkO3l95MA/640?wx_fmt=gif&tp=webp&wxfrom=5&wx_lazy=1)

解脱了那些动物，保护了环境，说起来还是挺美好的，但总觉得哪里不对。目前因成本，推广不开，我相信会有那么一天，端上我们的餐桌。

3、[小学生发现《西游记》的“大漏洞”](https://mp.weixin.qq.com/s/GbfsFEA2zvdq-ILRkX-Rgw)

![小学生发现《西游记》的“大漏洞”](https://mmbiz.qpic.cn/mmbiz_jpg/oq1PymRl9D665ctsuv8eMblmg52yVCSsT28jFXWeAgr7FMuZ2yEXC5DBrsaYFD1wOlHsDRoU4fRpmPxkxqB1icQ/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

读者根据《西游记》中不同故事里宴会上的菜名，推断出不够严谨的细节，角度新意。

4、[月球背面](https://mp.weixin.qq.com/s/Ya5p_MUxrVDmtOvUMwUnJQ)

嫦娥四号月球探测器载着我们的探月飞天梦想，奔向了人类目不可及的月球背面。

## 教程

1、[**网络写作的规范**](https://github.com/ruanyf/document-style-guide)

>注：[为什么文件名要小写？](http://www.ruanyifeng.com/blog/2017/02/filename-should-be-lowercase.html?tdsourcetag=s_pctim_aiomsg)

之前一直纠结于文件的命名，这篇文章，很好的总结了应该怎么命名比较好。

## 资源

1、[Kindle送书](https://mp.weixin.qq.com/s/WqkQlE4FIAB2F-b9LLjI2g)

![三体](https://imgsrc.baidu.com/baike/pic/item/5bafa40f4bfbfbed771e091575f0f736afc31f10.jpg)

包含了

> * 《与罗摩相会》
> * 《下雨天一个人在家》
> * 《少年维特的烦恼》

2、[markdown中的数学公式](https://cloud.tencent.com/developer/article/1337897)

markdown有原生的数学公式用法，这里贴的是一个教你将数学公式做成图的网址。

3、[也谈钱](https://mp.weixin.qq.com/s/ivJAB8qTJMGUo71ODN3TBg)

推荐的关于理财的书籍有点多，所以推荐必看作者的[读书笔记](https://mp.weixin.qq.com/s?__biz=MzUzNjE3NzQ3Nw==&mid=2247484121&idx=1&sn=f4beabbdf1a80943e03ea7ca626aa18e&chksm=fafb7cf3cd8cf5e5f7501780c1c6fe41ebb4759b5ed35c63b576ac786dba830a7020f943ead6&scene=21#wechat_redirect)去其糟粕，取其精华。

## 工具

1、[小黄条](http://www.6fcsj.com/)

还不错的一款备忘录，嵌入桌面式，简介，跨平台。

2、[桌面管理腾讯](https://pc.qq.com/detail/5/detail_23125.html)

![桌面管理腾讯](https://pc3.gtimg.com/softmgr/image/v3/05/E4D8B4983354C0804B2513E40D6349FB14512F2B.png)

一款管理桌面的软件，老板还行，不过据说新版功能鸡肋比较多，而且耗费性能。

3、[Web2Desk](https://desktop.appmaker.xyz/)

可以把你的网页生成本地 .exe 应用的网站。我在学校的时候就想着能做一个 webApp 的管理软件，可惜没做出来，非常看好网页应用，相信随着 5G 的来到会使其变得更有前景。

4、[袋鼠输入](https://www.52pojie.cn/thread-832158-1-1.html)

一款很有趣的软件，主要可以用手机操作电脑，目前功能有

>* 可以手机遥控电脑操作，类似于无线鼠标的操作，并非一般意义上的远程控制软件
>* 通过手机实现电脑语音、手写输入
>* 手机控制电脑视频、PPT播放

6、[百度脑图](http://naotu.baidu.com/file/8099e4df6105b8a486c58b2ee997c9cc)

在线的思维导图编辑器，好处是只要有网的地方都能编辑，简介，但是功能比较少。

## 视野

1、[《上帝掷骰子吗》](https://baike.baidu.com/item/%E4%B8%8A%E5%B8%9D%E6%8E%B7%E9%AA%B0%E5%AD%90%E5%90%97/253484)

讲科学和历史的一本很有趣的书。

![上帝掷骰子吗](https://gss3.bdstatic.com/7Po3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=3f17747eb63eb13544c7b0bd9e25cfee/58ee3d6d55fbb2fbfc0b0084454a20a44623dc00.jpg)

## 金句

1、

是非审之于心，毁誉听之于人，得失安之于数。（截取于 对联）

2、

让美丽的夜空带我们踏过平庸。（回忆 南仁东 的一篇文章）

![FAST](https://gss1.bdstatic.com/9vo3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike80%2C5%2C5%2C80%2C26/sign=d56653d1e1f81a4c323fe49bb6430b3c/4034970a304e251f4c9f055dad86c9177f3e532f.jpg)

3、

彩色的路标，禁止通行的警告。（日光倾城）

4、

感谢这一生，没白费。（遗书）

## 正文到此结束

脆弱又短暂，转眼又一年。看到我们国家顺利发射了嫦娥四号还是很激动。遥想当初，刚上初中没多久，学校还专门放假给我们播放首次发射载人航天的直播······

现在这种自豪感，还存在于心，让我想起了一句话“为中华崛起而读书”。为人类幸福而读书。