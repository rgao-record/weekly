# 八月-立秋

## 成长-鸡汤

### [废掉一个人最隐蔽的方式，是让他忙到没时间成长](https://mp.weixin.qq.com/s/9e1YtKRB10vxxHrNa3kf1Q)

一份好工作的标准里必须有一条
你有时间成长：

- 有钱
- `有闲`
- 有成长

总的来说，坚持学习非常重要。老生常谈了···

## 技术-JAVA-X

1、[Java 必须掌握的 20+ 种 Spring 常用注解](https://mp.weixin.qq.com/s/kUkUvn7JPyC8WAiD_MTdSQ)
springMVC spring

2、[这才是微服务划分的正确姿势，值得学习！](https://mp.weixin.qq.com/s/dcbx6QfYyD7oqcTDSiYing)

- 拆分后的维护成本要低于拆分前
- 考虑软件发布频率(按照8/2原则进行拆分)
- 基于业务逻辑(最基本且重要)

3、[IntelliJ IDEA 2019从入门到癫狂 图文教程！](https://mp.weixin.qq.com/s/kbFMtoG35rmciJPnMhLexQ)

好用的插件以及快捷键、常用配置介绍。

4、[MySQL 详细学习笔记 可以收藏啦](https://mp.weixin.qq.com/s/j-Tqx829elV7qKQScMtLxg)

真的详细，包括了增删改查、用户权限、约束、值类型等等的知识点。

5、[重量级！Maven史上最全教程，看了必懂](https://mp.weixin.qq.com/s/V7Q7HCHp1yI_h8NcYTg7Ww)

- 为什么要用 maven
- 什么是maven

## 游戏说

1、[小CP做游戏的新思路](https://mp.weixin.qq.com/s/qzjxQDnD9hHVyqlgZY5w7g)
- 设计最初如果不开新服，注意新老玩家的差距。

2、[四十万字剧本，7位跑团重度爱好者做了一款古典奇幻CRPG](https://mp.weixin.qq.com/s/S7sAPgOf-UpBTf3ErWtF6w)

3、[游戏AI：只是AI间的游戏，还是游戏的未来？](https://mp.weixin.qq.com/s/upuHt7Bm6tNj3av9BXiUWg)

4、[从《黑暗之魂1》的地图设计一览魂系游戏的创造之美](https://mp.weixin.qq.com/s/D623rPqSKKy4sgGAmBvyDg)
地图设计很惊艳！

5、[三国游戏的巅峰！ 窥探《全面战争：三国》的系统设计之美](https://mp.weixin.qq.com/s/1k0ZwtThApebKrVZSVwA1A)
- 动态人物关系图谱（很棒的设计）

6、[1200场对白场景，25000行对白文本，30小时演出时长 育碧是如何制作对白动画的？](https://mp.weixin.qq.com/s/W0nrfN7SpQdj30IezkJ49g)
自动化生成对话的脚本

## 很有意思

1、
宾语是你，吉下两点一口，又有欠字相依。

2、
任何瞬间的心动都不容易，不要怠慢了它。

3、
我说，亿万星辰不及你。

4、
只要你对别人有帮助，就一定能实现自己的价值。

5、
无论什么时候都会有你解决不了的问题。

6、
见识浅薄。
随处可见的垃圾榨干了生命。

7、
人这一辈子，其实只要抓住一到两次机会就够了，但也只需要一到两次打击，一辈子就毁了。

8、
消除是手段，连接是根本。让我们消除人生障碍，连接多彩世界

9、
伤心的时候听爱听的歌，快点让心情好起来吧！ ——伤心的人别听慢歌，靠谱么？

10、
- 表达每一章的逻辑脉络
- 带走书中亮点
- 自己的心得看法
- 发现与其他书的亮点