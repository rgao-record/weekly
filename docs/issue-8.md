# 分享第八期

上个月就像死了一样，焦虑往往是时间和物质的集合···

[听说你，很焦虑](https://mp.weixin.qq.com/s/k6ST0eVL2qR8-QrSBxCXSw)

## 技术

1、[泡妞与设计模式](https://blog.csdn.net/u012371712/article/details/81194121?tdsourcetag=s_pctim_aiomsg)

通俗易懂的讲解设计模式。

2、[闭包，懂不懂由你，反正我是懂了](https://www.cnblogs.com/frankfang/archive/2011/08/03/2125663.html)

群里老哥说很多面试官都喜欢问这个。函数嵌套

3、[Java 核心编程技术干货，2019 最新整理版！](https://mp.weixin.qq.com/s/caQB7fmuWvqpdE5-AMSirg)

长期更新，注意看下多线程。

4、[Java关键字 及其 更详细介绍](https://mp.weixin.qq.com/s/7TElbFUAtsSMg8YE4fI6xg)

很全，可以当资料查阅

5、[详细的Docker入门总结](https://mp.weixin.qq.com/s/Mcg385F12YdeFZTJHE5mBA)

讲 docker 的可以看看

6、[为什么 Redis 单线程能支撑高并发？](https://mp.weixin.qq.com/s/O-2w0jZ_T6ChBPXzjukQVQ)

Redis 底层原理

7、[面试官问我网络协议，我该怎么办？](https://mp.weixin.qq.com/s/nPJ7FMeqjsFfN7tANZGj_w)

HTTP 网络协议

8、[codelf](https://unbug.github.io/codelf/)

变量命名的神器

[漫画：什么是二分查找？](https://mp.weixin.qq.com/s/HXF6rWJPeGyLLhiSa7zdog)

通俗易懂讲算法

[Redis常见面试题](https://mp.weixin.qq.com/s/x4FeWzXfnptRBSm3-1mLIg)

redis 的常见问题

[我用Creator 做游戏这段时间的游戏优化心得](https://forum.cocos.com/t/creator/69023)

游戏性能优化心得

## 资料

# 正文到此结束

时间本身是不存在的，公理是不用证明的--。
